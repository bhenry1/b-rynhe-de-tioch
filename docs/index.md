---
author: Bruno HENRY
title: 🏡 Accueil
---

# Modèle de site pour l'enseignement

## essai 1

![logo_2nde](assets/images/logos/Logo_Math_2nde.svg)

![chef](assets/images/Chef.svg)
![degrade](assets/images/Degrades.png)
![para1](assets/images/para_1.svg)
![para2](assets/images/para_2.svg)
![affine3](assets/images/affine_3.svg)
![treillis](assets/images/Treillis.svg)

![icone capytale Web](assets/images/icone.svg)
![icone capytale edit Web](assets/images/edit.svg)
![icone capytale ellipse Web](assets/images/ellipsis_vert.svg)
![icone capytale upload Web](assets/images/upload.svg)
![log capytale rect](assets/images/logo-rect-Capytale_fond_transp.svg)

Lien vers [l'essence](assets/sons/R_Devos_Le_prix_de_l_essence.ogg).
Lien vers [exemple taux](assets/sons/Seconde_exemple_taux.mp4).

!!! conclu "test"

    style conclu c'est ça !


!!! pied-piper "essai" 

    un peu beuacoup

!!! micro "inter"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et

!!! micro " "

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et

!!! coeur "On aime "
    ça et ça

## Essai 2

<img src="assets/images/logos/Logo_Spe1e.svg" alt="logo">

## Essai 3

![logo_html](assets/images/logos/HTML5_Logo.svg)

## Essai 4

![NMEA](assets/images/2page106.png)

![microbit](assets/images/microbit.gif)

![Rose_vents](assets/images/Windrose.svg)

??? info "Ce site a utilisé le modèle"

    😊 Ce site est un modèle de site pour **usage général**. Il en existe un autre pour un usage d'enseignement de l'informatique ([Modèle informatique](https://modeles-projets.forge.apps.education.fr/mkdocs-pyodide-review/){  :target="_blank"  }). Cela signifie que vous pouvez en faire une bifurcation [^1] (on dit aussi faire un «fork») afin de construire votre propre site.

    Vous trouverez un lien vers le [dépôt](https://forge.apps.education.fr/docs/modeles/site-web-cours-general){ .md-button target="_blank" rel="noopener" } et un lien vers un [tutoriel](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/){ .md-button target="_blank" rel="noopener" }.

    Ce modèle est un témoin des différentes possibilités présentées dans le tutoriel.

## Un modèle de base

Ce modèle se veut épuré mais présente les principales fonctionnalités pour vous permettre une appropriation rapide.
On trouvera :

- "Chapitre 1" avec un seul fichier, le plus simple possible, avec trois exemples d'admonitions (voir le tutoriel), 
et des exemples de formules de maths ou de chimie

- "Gros chapitre" contenant plusieurs fichiers, correspondant à des parties du chapitre.  

    - Dans Chapitre 2 - la Page 1 vous trouverez un exemple d'admonition d'exercice avec la soution cliquable
    - Dans Chapitre 2 - la Page 2 vous trouverez un exemple avec une image

    Ce chapitre est organisé grâce à un fichier `.pages`

- "Exemples de QCM" avec plusieurs QCM proposés

- Les tags utilisés
    

- "Crédits."

😊 Il existe beaucoup d'autres possibilités pour votre site que vous découvrirez dans le tutoriel.

👉 Vous pouvez désormais modifier le contenu des fichiers pour créer votre propre site web.


!!!info "Conserver et cacher des pages"

    Si vous voulez conserver certaines pages de ce modèles sans qu'elles ne soient visibles dans le menu, il suffit d'enlever leur nom du fichier .pages   
    Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite.

## Accueil à personnaliser une fois votre propre site réalisé

😂 Ne pas oublier de personnaliser cette page qui sera l'accueil de votre propre site.

Voir le tutoriel [Personnaliser l'accueil](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/01_demarrage/1_demarrage/#iv-personnaliser-la-page-daccueil-du-site-que-vous-avez-clone){:target="_blank" }


👉 Exemple d'accueil : 

# Bienvenue dans le cours de ...

Dans ce cours, nous traiterons de ...


[^1]: Voir le tutoriel : [tutoriel pour faire une bifurcation (un fork en anglais)](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/08_tuto_fork/1_fork_projet/){:target="_blank" }

