---
author: Bruno HENRY
title: Polices de caractères
tags:
  - caractère
  - police
---

## I. Polices libres :



!!! info "CMU"
    test
    [exemple avec la police CMU](a_telecharger/Exemple_CMU.pdf){ .md-button target="_blank" rel="noopener" }


[Exemple avec la police OpenDyslexic](a_telecharger/Exemple_OpenDyslexic.pdf){ .md-button target="_blank" rel="noopener" }

<div class="centre">
    <iframe 
    src="../a_telecharger/Exemple_CMU.pdf"
    width="80%" height="600" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
</div>

[memo caractère unicode](a_telecharger/memo_caracteres_speciaux.pdf){ .md-button target="_blank" rel="noopener" }

<div class="centre">
    <iframe 
    src="../a_telecharger/memo_caracteres_speciaux.pdf"
    width="80%" height="600" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
</div>

![](a_telecharger/R_Durer.svg)  

![](a_telecharger/R_Durer_empattement.svg)


??? note "se déroule en cliquant dessus"

    Ma note indentée
    Seconde ligne


??? tip "Astuce"
    première ligne
    Ma belle astuce indentée



### 1. Sous paragraphe 1

Texte 1.1

### 2. Sous paragraphe 2

Texte 1.2

## II. Paragraphe 2 : Quelques formules

Utiliser LaTeX

### 1. En maths

Une suite : 

$$
\left\{
        \begin{array}{ll}
            u_0 = 3 \\
            u_{n+1} = 5 \times u_n+2\\
        \end{array}
\right.
$$

Ajouter ses commandes :

$$
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\norm{\vec{v_C}} = \frac{\sqrt{(x_D - x_C)^2 + (y_D - y_C)^2}}{\Delta t}
$$

La norme du vecteur ${\vec{u}}$ se note $\norm{\vec{u}}$.

### 2. En chimie

$$
{CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}
$$

$$
^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}
$$

On peut tout mettre en ligne : d'abord cette formule ${CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}$ 
puis celle-ci :  $^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}$

