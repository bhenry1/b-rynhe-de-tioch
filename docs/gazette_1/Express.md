 
!!! info "Rapidement, quelques informations"

    - Il existe une [**liste de diffusion**](https://groupes.renater.fr/sympa/subscribe/capytale) de Capytale où il est rare de ne pas avoir de réponse à ses questions.
        - On s'inscrit en suivant le lien.
        - On reçoit des courriels chaque fois qu'une question ou des réponses sont postées sur la liste. 
    - On peut accèder à la documentation de Capytale via [**cette page**](https://capytale2.ac-paris.fr/) qui contient également 9 vidéos, dont 7 de démonstration de moins d'une minute, pour autant de types d'activités. 
    - [MathAData](https://mathadata.fr/fr/lycee/ressources) le programme de recherche-action à but non lucratif de l'ENS et du collège de France utilise les activités notebook séquencé de Capytale et les partage dans la bibliothèque.
 

!!! Abstract "Sommaire du prochain numéro "

    - Créer une activité Geogebra dans **Capytale**
    - Focus sur une activité **CodePuzzle**
    - Express

 