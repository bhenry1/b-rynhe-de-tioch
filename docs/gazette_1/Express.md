 
!!! info "Rapidement, quelques informations"

    - Il existe une [**liste de diffusion**](https://groupes.renater.fr/sympa/subscribe/capytale) de Capytale où il est rare de ne pas avoir de réponse à ses questions.
    - On peut accèder à la documentation de Capytale via [**cette page**](https://capytale2.ac-paris.fr/) qui contient également des vidéos de démonstration de moins d'une minute, pour 7 types d'activités. 
    - …

!!! Abstract "Sommaire du prochain numéro "

    - Créer une activité Geogebra dans **Capytale**
    - Focus sur une activité **CodePuzzle**
    - Express

 