# Focus sur une activité de la Bibliothèque

<!-- 824 mots : 4 minutes maxi
L'entretien est en cours et va évoluer-->



## Découverte du théorème de Pythagore

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_15005f7e725ee10ad00cb84459192b35.png)

Voila comment se présente dans la bibliothèque, l'activité *Geogebra* créée par Gaëlle THRO.

Et voici ce qu'un clic sur le titre en bleu affiche :

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_51a2af531e4f0d1250d10f56b0add244.png)


!!! coeur "On apprécie"
    - Les consignes données dans l'interface de Capytale sont claires pour les élèves,

        Elles présentent un point de vérification qui évitera de ne pas pouvoir conjecturer du fait d'une construction incorrecte.
    
    - Cette activité peut être clonée depuis la bibliothèque et réutilisée telle que vous la voyez ci-dessus.

!!! heart "On apprécie"
    - Les consignes données dans l'interface de Capytale sont claires pour les élèves,

        Elles présentent un point de vérification qui évitera de ne pas pouvoir conjecturer du fait d'une construction incorrecte.
    
    - Cette activité peut être clonée depuis la bibliothèque et réutilisée telle que vous la voyez ci-dessus.
 

L'activité étant présentée, vous allez en apprendre plus ci-dessous.

## Entretien avec l'auteure Gaëlle THRO


!!! micro "Peux-tu te présenter rapidement ?"
    - Je suis professeur de mathématiques depuis 17 ans.
    - J'ai travaillé un an en lycée dans l'académie de Strasbourg. 

        Depuis, je suis en collège dans les académies de Créteil, puis de Versailles et finalement de Nantes.
    - Je suis au collège du Lion d'Angers depuis 2016, c'est l'établissement où je suis restée le plus longtemps.


!!! micro "Quel est ton rapport au numérique de façon générale ?"  
    - Durant mes études, j'ai suivi l'option informatique en prépa MPSI puis MP. 
    - À mes débuts, j'ai assez rarement fait pratiquer le numérique à mes élèves, mais je l'ai utilisé dans mes préparations de cours, notamment pour montrer des configurations géométriques.

        Ces dernières années, mes élèves passent 2 à 3 heures face à un ordinateur par  période entre 2 vacances, sur des activités liées au programme. C'est finalement autour de 10% du temps.
    - Je suis souvent attirée par les nouveautés et finalement, je ne garde que ce qui a un bon rapport, temps de création sur temps d'utilisation avec les élèves.


!!! micro "Comment as-tu rencontré Capytale la première fois ? "  
    - J'ai vraiment rencontré Capytale durant l'année 2023-2024 en tant que maman. Ma fille en 6^e^ avait été absente lors d'une séance que le professeur lui a demandé de rattraper à la maison, et ça a poussé ma curiosité.
    - J'ai testé durant l'été 2024. Je savais qu'à la rentrée j'aurais des 4^e^ que je n'avais pas eu depuis des années. J'avais donc beaucoup d'envie de préparer de nouvelles activités.
    - J'en ai également discuté avec un collègue de technologie qui utilise Capytale.


!!! micro "D’où vient l’idée de cette activité ?"  
    - Je trouve formateur de conjecturer le théorème de Pythagore, mais les figures faites sur cahier ne sont jamais suffisamment précises pour cela. 
    - Un logiciel de géométrie est indispensable. 


!!! micro "Comment utilises-tu cette activité ?"  
    J'ai utilisé cette activité en classe avec deux classes de 4^e^, durant la première période de cette année.

    - Les élèves étaient deux par ordinateur. L'activité a occupé une demi-séance et quelques minutes de la séance suivante. 

        Ils ont été très autonomes. Une fois l'activité ouverte, je n'ai eu aucune question.

    - J'ai passé du temps à réapprendre à certains élèves comment ouvrir une session sur l'ordinateur, puis se connecter sur l'ENT. L'activité était donc finie pour les élèves autonomes avec l'outil informatique, et pas commencée pour d'autres. 

        J'ai donné d'autres activités numériques à ceux qui avaient fini.

    Au cours suivant, nous avons fait un bilan de l'activité, tandis que je leur projetai une sélection de travaux d'élèves : 

    - avant ce cours, j'avais visionné et annoté les travaux sur Capytale, ce qui m'a permis de montrer les plus intéressants, 

        et nous avons ensuite écrit un bilan de l'activité.


!!! micro "Que t’as apporté Capytale ?"  

    J'ai apprécié que Capytale permette de tout préparer et de tout rendre disponible au même endroit :

    - les espaces graphique et tableur de Geogebra configurés côte à côte avec la consigne au-dessus, lors de l'arrivée sur l'activité.
    - Ne pas avoir besoin de photocopier des consignes est un avantage !

    Capytale m'a en plus permis 

    - de sauvegarder facilement les travaux de mes élèves, tous au même endroit et correctement nommés,
    - de les visionner chez moi,
    - de les annoter facilement 
    - et de projeter ma sélection au cours suivant.

    L'autonomie des élèves grâce aux consignes à l'écran a également été très intéressante,


!!! micro "As-tu rencontré des difficultés ?"  
    Non, si ce n'est la découverte de l'outil : c'est la première activité que j'ai créée.
 
## Testez l'activité

Si vous n'êtes pas identifié, vous devez le faire en cliquant sur son nom de votre ENT. 

Quelques navigateurs refuseront de vous connecter depuis la présente page. Il suffit alors de se connecter à votre ENT dans un autre onglet de votre navigateur et de recharger la présente page.

Au pire c'est l'occasion de visiter la [bibliothèque de Capytale](https://capytale2.ac-paris.fr/bibliotheque) :

<iframe
    src= "https://capytale2.ac-paris.fr/web/b/3795927"
    width="90%" height="800" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>

