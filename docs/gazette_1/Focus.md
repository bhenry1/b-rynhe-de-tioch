# Focus sur une activité de la Bibliothèque

<!-- 824 mots : 4 minutes maxi
L'entretien est en cours et va évoluer-->

Dans cette rubrique, nous vous présentons une activité de la bibliothèque que nous estimons réussie. 

## Conjecturer Pythagore

L'image ci-dessous montre l'activité *Geogebra* créée par Gaëlle THRO, telle qu'elle se présente dans la [bibliothèque](https://capytale2.ac-paris.fr/bibliotheque).

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_15005f7e725ee10ad00cb84459192b35.png)


Et voici ce qui s'affiche après un clic sur son titre en bleu ci-dessus :

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_51a2af531e4f0d1250d10f56b0add244.png)


!!! heart "On apprécie"
    - La description dans la bilbiothèque est en adéquation avec son contenu.
    - Les consignes données dans l'interface de Capytale sont claires pour les élèves.

        Elles présentent un point de vérification qui leur évitera de ne pas pouvoir conjecturer du fait d'une construction incorrecte.
    - Dès qu'elle a été clonée depuis la bibliothèque, cette activité peut être distribuée à des élèves.
    - La présentation de l'activité est soignée.

 

L'activité étant présentée, vous allez en apprendre plus par son auteure.

## Entretien avec l'auteure Gaëlle THRO


!!! micro-q "Peux-tu te présenter rapidement ?"
    - Je suis professeure de mathématiques depuis la rentrée scolaire 2007.
    - J'ai travaillé un an en lycée dans l'académie de Strasbourg. 

        Depuis, je suis en collège dans les académies de Créteil, puis de Versailles et finalement de Nantes.
    - Je suis au collège du Lion d'Angers depuis 2016.


!!! micro "Quel est ton rapport au numérique de façon générale ?"  
    - Durant mes études, j'ai suivi l'option informatique en prépa MPSI puis MP. 
    - À mes débuts, j'ai assez rarement fait pratiquer le numérique à mes élèves, mais je l'ai utilisé dans mes préparations de cours, notamment pour montrer des configurations géométriques.

        Ces dernières années, mes élèves passent 2 à 3 heures face à un ordinateur par  période entre 2 vacances, sur des activités liées au programme. C'est finalement autour de 10% du temps.
    - Je suis souvent attirée par les nouveautés et finalement, je ne garde que ce qui a un bon rapport, temps de création sur temps d'utilisation avec les élèves.

        Je vais garder Capytale. Je suis vraiment très satisfaite de la rapidité de création des activités Mathaléa, de la possibilité pour les élèves de retenter des exercices (une fois la correction affichée, avec un nouvel énoncé) et de la disponibilité des résultats côté enseignant. 
    - J'ai depuis peu une élève allophone d'un niveau très différent du reste de la classe et cela me permet de différencier très facilement.


!!! micro-q "Comment as-tu rencontré Capytale la première fois ? "  
    - J'ai vraiment rencontré Capytale durant l'année 2023-2024 en tant que maman. Ma fille en 6^e^ avait été absente. 

        Son professeur lui a demandé de rattraper une séance sur Capytale à la maison. 

        Comme il s'agissait de découvrir Géogebra, j'ai dû l'aider un peu et l'interface de Capytale a piqué ma curiosité.

    - Pendant l'été suivant, je me suis lancée.

        Je savais qu'à la rentrée j'aurais des 4^e^ que je n'avais pas eu depuis des années.

        J'avais donc beaucoup d'envie de préparer de nouvelles activités.
    - J'ai également discuté avec un collègue de technologie qui utilise Capytale.


!!! micro "D’où vient l’idée de cette activité ?"  
    - Je trouve formateur de conjecturer le théorème de Pythagore, mais les figures faites sur cahier ne sont jamais suffisamment précises pour cela. 
    - Un logiciel de géométrie est indispensable. 


!!! micro "Comment utilises-tu cette activité ?"  
    J'ai utilisé cette activité en classe avec deux classes de 4^e^, durant la première période de cette année.

    - Les élèves étaient deux par ordinateur. L'activité a occupé une demi-séance et quelques minutes de la séance suivante. 

        Ils ont été très autonomes. Une fois l'activité ouverte, je n'ai eu aucune question.

    - J'ai passé du temps à réapprendre à certains élèves comment ouvrir une session sur l'ordinateur, puis se connecter sur l'ENT. L'activité était donc finie pour les élèves autonomes avec l'outil informatique, et pas commencée pour d'autres. 

        J'ai donné d'autres activités numériques à ceux qui avaient fini.

    Au cours suivant, nous avons fait un bilan de l'activité, tandis que je leur projetai une sélection de leurs travaux : 

    - avant ce cours, j'avais visionné et annoté les travaux sur Capytale, ce qui m'a permis de montrer les plus intéressants, 

        et nous avons ensuite écrit un bilan de l'activité.


!!! micro "Que t’as apporté Capytale ?"  

    J'ai apprécié que Capytale permette de tout préparer et de tout rendre disponible au même endroit :

    - les espaces graphique et tableur de Geogebra configurés côte à côte avec la consigne au-dessus, lors de l'arrivée sur l'activité.
    - Ne pas avoir besoin de photocopier des consignes est un avantage !

    Capytale m'a en plus permis 

    - de sauvegarder facilement les travaux de mes élèves, tous au même endroit et correctement nommés,
    - de les visionner chez moi,
    - de les annoter facilement 
    - et de projeter ma sélection au cours suivant.

    L'autonomie des élèves grâce aux consignes à l'écran a également été très intéressante,


!!! micro "As-tu rencontré des difficultés ?"  
    Non, si ce n'est la découverte de l'outil. C'était la première activité que je créais. 

    - je n'étais pas sure de l'affichage en mode élève. 
    - Comment s'afficheraient les 2 espaces Géogebra et la consigne ?
    - J'ai pu me rassurer avec le compte de ma fille !
 
## Remarque

On n'y pense pas nécessairement lorsqu'on commence :

!!! success "Être son propre élève"

    - Dans capytale, vous pouvez tester votre activité avec le rôle élève, simplement en utilisant le code ou le lien que vous donnez aux élèves.
    - Vous pourrez supprimer votre copie élève en utilisant le menu `Supprimer`.

        **Attention** de ne pas supprimer l’activité !

![copies enseignant et eleve](../assets/images/Activite_prof&eleve.png)

## Testez l'activité de Gaëlle

Le plus simple est de vous rendre dans la [bibliothèque de Capytale](https://capytale2.ac-paris.fr/bibliotheque).

Vous écrivez « Thro », le nom de Gaëlle dans la zone de recherche, puis cliquez sur le nom de l'activité.

