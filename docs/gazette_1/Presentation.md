# Fonctionnalités de Capytale

*Capytale* est une plateforme dont l'objectif est de  **faciliter l'utilisation** des logiciels liés aux Sciences, Technologie, Ingénierie, Arts et Mathématiques (S.T.I.A.M.) ainsi que la **gestion des copies** numériques. 

Depuis 2020, *Capytale* permet à des centaines de milliers d'enseignants et d'élèves d'accéder à des outils sans installation, tout en respectant le RGPD. 

!!! success "Simplicité 1" 
    Capytale utilise l'identification de l'espace numérique de travail (ENT) de votre académie. 

    Chaque utilisateur dispose d'un **espace personnel** hébergé à Paris.
    


![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d7d8b68beb9b01deaac0db2684d8e75b.png )

*Capytale* permet de **créer**, **distribuer** et **évaluer** des activités pédagogiques, gérant ainsi toutes les étapes d'une activité S.T.I.A.M. 

!!! success "Simplicité 2"
    *Capytale* ne requiert **aucune** installation et les versions des logiciels à disposition sont **identiques** pour tous.

    Fonctionnant dans un simple navigateur internet, *Capytale* est utilisable en classe ou à domicile par les enseignants et les élèves.

De plus, *Capytale* permet le partage entre enseignants grâce à une bibliothèque collaborative contenant des milliers  d'activités sous licence Creative Commons, ce qui facilite la réutilisation de ces ressources pédagogiques partagées.

## Les trois étapes de la vie d'une activité

### 1. Créer une activité

Trois possibilités s'offrent à vous : 

- Cloner une activité partagée dans la bibliothèque par un autre enseignant.
- Réutiliser une activité déjà utilisée avec une autre de vos classes. 
- Créer une nouvelle activité parmi les 30 types supportés : *Python*, *GeoGebra*, *Codabloc* la version libérée de *Scratch*, *MathAléa*, *MicroBit*…

![](../assets/images/etape1.png)

### 2. Distribuer à la classe
 Chaque activité de votre espace dispose d'un code, d'un QR code et d'un lien de partage. 
 
 Chacun permet aux élèves de créer une copie personnelle de l'activité dans leur espace.

![](https://capytale2.ac-paris.fr/media/etape2.png)

### 3. Évaluer les travaux 
Une fois les copies réalisées par leurs élèves, Les enseignants peuvent consulter et évaluer (parfois automatiquement) les copies des élèves.

![](https://capytale2.ac-paris.fr/media/etape3.png)

Ci dessous, l'extrait vidéo décrit ces 3 étapes. Les 3 chapitres sont accessibles directement via cette icône <i class="fa fa-list fa-fw"></i>.

<iframe src="https://podeduc.apps.education.fr/video/80593-cycle-dune-copie-dans-capytalewebm/?is_iframe=true" width="800" height="450" style="padding: 0; margin: 0; border:0" allowfullscreen title="Cycle d'une copie dans Capytale.webm" ></iframe>

## La bibliothèque collaborative
### Description
!!! info ""
    La bibliothèque est un espace de **partage**.

    - Elle est accessible en un clic  
    - Elle contient plus de 6 000 activités éducatives partagées par des enseignants,
    - La licence [Creative Commons By-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/) permet de modifier une activités clonées. 

Grâce à des filtres, il est facile de trouver des activités adaptées à différentes situations. 

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_71b4139747d2eef7f558ea3961e917c5.png)

Les enseignants peuvent

- **visualiser** une activité d'un clic sur son nom en bleu et si elle convient,
- la **cloner** en un clic pour l'intégrer dans leur espace personnel de travail. 
 
!!! success "Simplicité 3"
    La bibliothèque permet d'obtenir **rapidement** une activité utilisable avec les élèves.



### Illustration à partir d'une activité
Nous ne sommes pas dans la bibliothèque, mais voici ce que vous y verriez pour l'activité `Influence des coefficients d'une fonction affine` :

!!! info ""
    Elle ne demande pas aux élèves de programmer.

    Elle utilise la possibilité de séquencer qu'offre un notebook python.
    
    - Une fois diffusée aux élèves, l'activité ne leur apparait qu'au fur et à mesure de la saisie des `clés`.
    - Pour faciliter son exploration, la bibliothèque affiche l'activité intégralement. 
    Vous pouvez ci-dessous l'explorer comme vous le feriez depuis la bibliothèque.



<iframe 
src="https://notebook.basthon.fr/?kernel=python&ipynb=eJztWs1yG8cRfpUpkFUkV1iAP5LsUBR9kMSKU1SsJHYuhooa7A6AkWZnVzOzICiVqnTKzy2VU3LLKTZzc8pvgLsfQk-Sr2d2gQVEQbJCSj6ILsG789vd0_3117v7vJUIpWxr_9vnrUw4nnLHW_vPW6lQuOsr0dofcGVFuyVS2Wx40fYzT9xZgaZWxs2TND_VrXbL5qVJqO0gEdoJc9jTDH89vca-0YKNpHW5NIKlG1IPVCl0Inr6oDsbjJtUjlmiuLW3ey2uhHHM_8an3Giph70Ws-5MCfT2c5MKEysxcPs3igmzuZIpW_uc30z3-reqXsNTWdp9hgG3ei2_xR_z0mJRJZ6xvG-FGQvDSkg3kMMSsgnHoiidnid5OTbSRBFTwjIz_ZH-9zgvp-eWFdz41iQXg4FMJOS3LOXaMrWBmUa60mvpl8114mSuGcdQLToHXajoBVljf8izjMMgdBezbx-Y6TlXZOmHm2vzmy2yINkRQ35TFmewFfqrqy0_85jX4ksyJceOY1rkmMehPW60-_UwaafDjkVTB7aerTPM2unExyJudMToCBvtdtj0JenvpGGpWNDXm20sVF4U03MRRVhptxNPX8ZheJyKuDE8bg7eminIO3AVVuSQN2yTlErihPxivBN_o2PfGTc657P7fjbadJjQ9xPoPoi_t1L8AeyTG2nfRfrG2HcSfu_nCr_3mvDXO8tRNBMH46_TaBHXvXGjN0w_khrD8Ls1i8o1Nvey0LDGKr_q6eMNocfS5FqLjJzjaSnY2MfOGKFj6Ypi4EyUiD3BNHwZi0RRVK2AFif6ef4ETW1EGGNpnpR-qV4rkxMnEMxJDr_UHG1pyZxAIxOTQsmEOzmgWERzkuOkHpy5EYIoisQEepUej6KosxIzbJkkwto3YAZrgMZe8tnNvXQFaFCA8el3kGDE-zAq8I8OoJZGmIhtQspIwfTRFmlba08qWhyKYNN_NT2Ps8KITE6_x0kSnJZKtElhjLJcWow6sAXXhwdP-umhv5xpkeQqN_unI-nErT5PngxNXuo0Ds19hRaofHjQP2Sv_vwdu88fs4Nu__CgG9br-gXDDbtW79J__43Yqz_9l93TDq4k2NIus23Z49I-LTegnSKs0nSwtYk6lARmqLjGLkSzJegunVQS6B2W84OH0_N8KPqGs0QiXK13UQQTgKZQODzyMo_apbEC_9qI1NIA2s-LXKdYgMOT4eWW4NriSjJbYmftOiucDJksf3tW2tv5bPvzwQoH6-kHJAw8gbfnXi6eUf7htYOQ4_homCvYCcFc2-9LhViEBYrcWtkPM4L-iTAL2vvgwkojroe-i1E-Z-mrl_-cYyKslyj5tCTT2dIbO-GWhDBGWDIbejqtF-1FCuFMCYdPZwTC8SHxjFbfCP7EI2Dr4RKLILWaDGJg8oydjDmsx336lFmRw-gRYMr3fRkQoZNKC-3O6v5ff33_uKfpd7PXov8O5MBwIJNNTK4UKAQOSdOJOen8iR0tJmi2z9iMncBCi1meTtokmDVyrrD73e7p6WlnKILfdXIz7GYcLiu56oZ9uzLtPr7xdDQUZ2fdU5m6UfdXn93ojoQcjlz3xvZeN_hE93P_17UD2-_CfqJrs37X269rXeNqVF1yWV_YWa-p20xSXai07kufVVeJU-EKuniBoA1EKia4D2KhAYL5hkW33t8ODgt3C9odBhtv4eTERMBhYccTMCcc8P5Ou5WXrihdoJjhuj7uMFqcwItK5d4wfeZOyAtdHLLURC2XDr5DZw0m91gkCEnHtic7uzd3b17fPsSqfubIZYomfnKEK3QEYEATAl68eLgMCpdRVyA5XMxarx6gY6Dy3QpJPSqLGkq9AEBTh-xI1KisSBxkA4RinJVDHbB7UXDhfK9AsiHuEI05_IU8EMujwSFkRCBZdRFhK9-0bH3Cer2MF2B7LJtcK9Y7JOId2sADYeBsAd1jGEERbUtWEV72RVDyDhZQ0_Mq92CrYmSA-bZKlHQ1Tz_7rCaT4TfkoeWE8A4ne7dRDcCgpBHLS9aQEK2bE_bqL39nP_2wtc72w6GAiVhipup1n7iw_mJI75QdJfhlGwfiXGMI9b16-W_Wfu-V9fR8yFct_XqyvJR6-8qdP7ATsHWpS9CFKAJ5bcSCDVWPVIrq4wb_CqUyy3IXCNUGGSsZeWoRyCbzdJKEA9RiP0TRUNSsEXwxmf7opucVZ_bsgxpzrGFqTvI6W-xUjIhdBTdJlDjZYbdRxuBEkf-wrXUnaN3ZvDAT6lKpZjK8KmhEcX4fnBg8tqpBvXlWVOg9fdc_tIAFhZ7-xzVJdJsh6JTw3quBagdkhcNX__gbu6hGWDs62sZfr3XrsBnJs2rAT2aIFDD_EFQNJuqrykKYjEJGL9DSClrXgQavPV9ZH2xOtm6zjE3YtYIRAK55I3CfIYIFBnIiPDB9iARxDMSo_DTLUznwVSxi34M7NKE6OIqqNBFSSJAyzDGiAMQiHbgwbWh4MZJkrfTdUoCPMAKaKJLaJxPtsE2nTl-LyM4rYJ_hejNsLw3X50dx2UrOVPRI_Qa7e__BYLXhwcEGfyYkvhIcDg7Y9w7Y4ADJgjtS2m-6sXdgVhCJmRnLOwlQj2T3kyod7YKGtlKxzTIuCWvDkBA8WXgqKSh4PkABG7PflUIFY5O_k_pwsxkJWsihhY_qmVm-uDQPxd17OOlxlerJW8ipRHsem3M3Cx6WmlwisfPw8CDJYRAAJLDOsk2f4W_R71aboTm54KHwAs8p1mfEIC2XN_xEFeZUIYqgZIn5qwlBnfavKuvvXpT1dz9u1t97e9Zf4Pl0SF8hfwdERAFuvVfa3HgQqZ7QLXHbsJqoHvtSP7L1IjtvAnWDqPvsE0W_zeuCoiqH1uMA5ZQOkun3hFM__bC--oHupbnojPa84clhaSlTLxAgyP42DkQM6OiIONDR3DBLFOi9GRCM82YKNLe350GLiBieqpKjvMaMll6BNM-TfZCa-mNSpmY1eSFz6lxaUvrIxOliXX-JBIqx4Kk_l0I19CPbXw2PCvj0iyJShApLTOr_YesfkAgt5A6vxicitEiEUlFO5PT7LFAiej8qnU_n1zv0gLBk30ZR_QWBf19d32xdNQ3au4gG7X1cGrTi7fiHillV8yT_6oziF8Gh6JVlUZi8MJJ83UdLSGhE05BwMj4U4Ynv9sXPYN-vDmLvVQktvP6jcHw3PTDN6-G1uJp4Zb_kQwRKEY09T4V_5HKpZ_mBz7GpyhUeJ0L2SOqrp_ssmJJO4t5gAPpS-oOgB_iWQ-khxzzi3pZovv-so5x_qZL3wcdLetngv9Gpvjp45utU6nh0hAqVPvHpsnvaiCHQx9BzaZwmjP6oU807ps_LwkD_ep57GgI7wzHAMsyQ6lnKkOOcIJ58AwbwOPbo6_mowEUedVi17B3fSCWbLyBSzw-qd_1vWu4-dyNIW7_sxGKsfvrye6FTb5wwzRk-5lJR-R30Dh9QsD7AHBSQvhepjm1jQJ_HxEL7Rxsixj-9sVQdzQ6KXhvSToHUVZuE70VoqxEv_UcwgV7MihmS74gKJJKP3sfWJ1QfzR36ToE-TCrNspArBXHk_6u3j9lXJRv4zVG8IprDyzXsovlYDrlDDbfwHUbrxcOFt6GtJ8LAt2whErrTPKMIKPx77D0ERfUq-6TqqL51oh4FBloCV2fDW1VouTxXfW7QPv0rYq_6uOM-cIbHdyseRmN1n0p1jkR8fX5zkkmdY-7ui_8BiXfpgQ"
width="90%" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>


!!! success "Pour finir"
    Si vous étiez dans la bibliothèque et que l'activité vous plaise, 

    - d'un clic sur `Cloner`, l'activité apparait dans votre espace Capytale personnel et
    - vous pouvez alors l'utiliser **ainsi** en la distribuant à vos élèves, ou la **modifier**, parce que vous préférez $ax+b$ à $mx+p$ par exemple. 
    - Cette **liberté** est garantie par la licence CC-By-SA. 


