# Fonctionnalités de Capytale

*Capytale* est une plateforme dont l'objectif est de  **faciliter l'utilisation** des logiciels liés aux Sciences, Technologie, Ingénierie, Arts et Mathématiques (S.T.I.A.M.) ainsi que la **gestion des copies** numériques. 

Depuis 2020, *Capytale* permet à des centaines de milliers d'enseignants et d'élèves d'accéder à des outils sans installation, tout en respectant le RGPD. 

!!! success "Simplicité 1" 
    Capytale utilise l'identification de l'espace numérique de travail (ENT) de votre académie. 

    Chaque utilisateur dispose d'un **espace personnel** hébergé à Paris.
    


![Résumé graphique](../assets/images/Resume.png )

*Capytale* permet de **créer**, **distribuer** et **évaluer** des activités pédagogiques, gérant ainsi toutes les étapes d'une activité <abbr title="Sciences, Technologie, Ingénierie, Arts et Mathématiques">S.T.I.A.M.</abbr> 

!!! success "Simplicité 2"
    *Capytale* ne requiert **aucune** installation et les versions des logiciels à disposition sont **identiques** pour tous.

    Fonctionnant dans un simple navigateur internet, *Capytale* est utilisable en classe ou à domicile par les enseignants et les élèves.

De plus, *Capytale* permet le partage entre enseignants grâce à une bibliothèque collaborative contenant des milliers  d'activités sous licence Creative Commons, ce qui facilite la réutilisation de ces ressources pédagogiques partagées.

## Les trois étapes de la vie d'une activité

#### 1. Créer une activité

Trois possibilités s'offrent à vous : 

- Cloner une activité partagée dans la bibliothèque de Capytale par un autre enseignant et éventuellement l'aménager.
- Réemployer une activité déjà utilisée avec une autre de vos classes. 
- Créer une nouvelle activité parmi les 30 types supportés : *Python*, *GeoGebra*, *Codabloc* la version libérée de *Scratch*, *MathAléa*, *MicroBit*…

![](../assets/images/etape1.png)

#### 2. Distribuer à la classe
 Chaque activité de votre espace dispose d'un code, d'un QR code et d'un lien de partage. 
 
 Les trois permettent aux élèves de créer une copie personnelle de l'activité dans leur espace, tout en vous en donnant un accès perpétuel en consultation aux copies de vos élèves.

![](https://capytale2.ac-paris.fr/media/etape2.png)

#### 3. Évaluer les travaux 
Une fois les copies réalisées par leurs élèves, les enseignants peuvent les évaluer (parfois automatiquement).

![](https://capytale2.ac-paris.fr/media/etape3.png)

Ci dessous, l'extrait vidéo décrit ces 3 étapes. Les 3 chapitres sont accessibles directement via l'icône <i class="fa fa-list fa-fw"></i>.

<iframe src="https://podeduc.apps.education.fr/video/80593-cycle-dune-copie-dans-capytalewebm/?is_iframe=true" width="800" height="450" style="padding: 0; margin: 0; border:0" allowfullscreen title="Cycle d'une copie dans Capytale.webm" ></iframe>

## La bibliothèque collaborative

!!! info " "
    La bibliothèque est un espace de **partage**.

    - Elle est accessible en un clic, depuis le bouton à droite de `Mes activités` dans le bandeau de Capytale.
    - Elle contient plus de 6 000 activités éducatives partagées par des enseignants.
    - La licence d'utilisation [Creative Commons By-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/) permet à chacun d'adapter dans son espace, une activité clonée depuis la bibliothèque. 

Grâce à des filtres, il est facile de trouver des activités adaptées à différentes situations. 

- Ci-desous, la saisie du texte *jeu de Nim* dans le premier champ de recherche a laissé 7 activités à l'affichage.
- Si besoin, il serait possible d'affiner en utilisant un ou plusieurs des 5 filtres supplémentaires.  

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_71b4139747d2eef7f558ea3961e917c5.png)

Les enseignants peuvent alors

- **visualiser** une activité d'un clic sur son nom en bleu,
- la **cloner** en un clic pour l'intégrer dans leur espace personnel de travail, **si** elle leur convient. 
 
!!! success "Simplicité 3"
    La bibliothèque permet d'obtenir **rapidement** une activité qui pourra être utilisée avec les élèves ou constituée la base d'une adaptation.

Ci-dessous, une capture d'écran lors de la visualisation d'une activité de la bibliothèque, permet de :

- lire les consignes de cette activité *Web*,
- localiser le bouton `Cloner` et différentes zones
	- celle de l' ![](../assets/images/edit.svg) <span style="color:DodgerBlue;">Éditeur de code</span>, 
		- l'onglet ![](../assets/images/code.svg)`index.html` où l'enseignant et ses élèves écrivent le code `HTML` et qui correspond à un fichier,
		- l'onglet ![](../assets/images/format_color_fill.svg)`style.css` où on écrit le code `CSS` et qui correspond à un autre fichier,
	- celle de la ![](../assets/images/public.svg) <span style="color:#d65108;">Prévisualisation</span> du site que ces différents codes constituent. 

![Web : recette de crêpes](../assets/images/Crepes.png)

