# \#![Capytale](../assets/images/logo-rect-Capytale_fond_transp.svg)1.1  

`#Capytale` est un périodique d'**information sur Capytale**, dont l'objectif est d'informer les enseignants, de démystifier la technique et de promouvoir quelques activités partagées dans la bibliothèque.

Nous aurions pu tout aussi bien l'appeler ***Le Monde de Capytale*** ou ***Info Capytale*** ou ***Regard sur Capytale*** ou même  ***La lettre Capytale***.

#### Le croisillon \#

Lorqu'il exécute du code, le langage python ne tient pas compte de tout ce qui suit un croisillon #.

Ce qui transforme la suite en un commentaire, en une **information** à l'usage des humains.

- La plupart des personnes lisent *hashtag* le caractère # et font ainsi une double erreur.
- D'autres confondent le croisillon # avec le dièse ♯ (*sharp* en anglais).

!!! info "Hashtag"
    Un *hashtag* est la concaténation, c'est-à-dire la juxtaposition sans espace

    - du caractère # (*croisillon* ou *hash* en anglais) **et** 
    - d'une étiquette (*tag* en anglais), d'une référence.

    Un *hastag* est donc un mot-clé qui étabit des liens cognitifs ou informatique.

