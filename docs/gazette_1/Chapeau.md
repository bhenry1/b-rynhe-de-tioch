# \#![Capytale](../assets/images/logo-rect-Capytale_fond_transp.svg).2025.04  

!!! success ""
    `#Capytale` est un périodique d'**information sur Capytale**, dont l'objectif est d'informer les enseignants, de démystifier la technique et de promouvoir quelques-unes des activités partagées dans la bibliothèque.

Cette publication a failli s'appeler ***Le Monde de Capytale*** ou  ***La lettre Capytale*** ou ***Regard sur Capytale*** …


Nous profitons de l'occasion pour parler d'un caractère. 

!!! info "Le caractère #" 
    \# s'appelle le *croisillon* en français et *hash* en anglais.

    - En langage python, le croisillon # transforme le texte qui le suit en un commentaire, en une **information** à l'usage des humains.
    - Des personnes confondent le croisillon # avec le dièse ♯ (*sharp* en anglais).
    - D'autres lisent *hashtag* le caractère # seul, alors qu'un *hashtag* est la concaténation (la juxtaposition sans espace)

         - du caractère croisillon (*hash* en anglais)  **et** 
         - d'une étiquette (*tag* en anglais), d'une référence.

         Un *hashtag*, une « étiquette-croisillon » est donc un mot-clé qui étabit des liens cognitifs ou informatique.

`#Capytale` se lit donc *hashtag Capytale*. 