
Rendu : https://bhenry1.forge.apps.education.fr/b-rynhe-de-tioch


Ceci est mon dépôt de fichiers accessibles via https.

That's all.

# Licence
Sauf erreur tout est sous licence CC-BY-SA.


# C'est un fork
Il permet aussi de faire des exercices Python car il est construit avec Pyodide Mkdocs Theme.
C'est ce qui a permis les QCM possibles sur ce site.
